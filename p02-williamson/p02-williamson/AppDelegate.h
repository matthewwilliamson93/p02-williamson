//
//  AppDelegate.h
//  p02-williamson
//
//  Created by Matthew Williamson on 1/29/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

