//
//  ViewController.h
//  p02-williamson
//
//  Created by Matthew Williamson on 1/29/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *t0, *t1, *t2, *t3,
                                                *t4, *t5, *t6, *t7,
                                                *t8, *t9, *t10, *t11,
                                                *t12, *t13, *t14, *t15,
                                                *score, *best, *title;

@property (nonatomic, strong) IBOutlet UIButton *up, *left, *right, *down, *reset;

- (void)swipeUp:(UISwipeGestureRecognizer*)gestureRecognizer;
- (void)swipeLeft:(UISwipeGestureRecognizer*)gestureRecognizer;
- (void)swipeRight:(UISwipeGestureRecognizer*)gestureRecognizer;
- (void)swipeDown:(UISwipeGestureRecognizer*)gestureRecognizer;
- (IBAction)upClick:(id)sender;
- (IBAction)leftClick:(id)sender;
- (IBAction)rightClick:(id)sender;
- (IBAction)downClick:(id)sender;
- (void)stepInGame:(int)is ie:(int)ie ii:(int)ii iu:(int)iu js:(int)js je:(int)je ji:(int)ji ju:(int)ju;
- (IBAction)resetClick:(id)sender;
- (void)move:(int)x1 y1:(int)y1 x2:(int)x2 y2:(int)y2;
- (void)merge:(int)x1 y1:(int)y1 x2:(int)x2 y2:(int)y2;
- (void) randUpdate;
- (void) updateLabels;

@end

