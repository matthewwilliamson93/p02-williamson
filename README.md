README

2048 project 2 for CS580U. Added gestures for swiping as the same as button click, and added gameplay feature, if you make wrong move with the button, instead of nothing happening you are penalized with another cell. This can be used to your advantage if you like. 

Name: Matthew Williamson

Email: mwilli20@binghamton.edu

B-Number: B00335701
